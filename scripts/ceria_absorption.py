#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.insert(0, '../lib')
import bhcoat_pyed
from scipy.interpolate import interp1d

# some constants
h = 6.626e-34
c = 3.0e+8
k = 1.38e-23

def planck(wav, T): # Planck's blackbody function
    a = 2.0*h*c**2
    b = h*c/(wav*k*T)
    intensity = a/ ( (wav**5) * (np.exp(b) - 1.0) )
    return intensity*1.0E-9

#show emission spectrum
temp = 1500.+273.


# import some data
nCeO2Dat = np.loadtxt("../data/nDataPatsalas950.txt",delimiter=",")
kCeO2Dat = np.loadtxt("../data/kDataPatsalas950.txt",delimiter=",")
# make it behave like a function
nCeO2 = interp1d(nCeO2Dat[:,0]*1E-9,nCeO2Dat[:,1])
kCeO2 = interp1d(kCeO2Dat[:,0]*1E-9,kCeO2Dat[:,1])

# samesies for solar data
solarDat = np.genfromtxt("../data/ASTMG173.csv",delimiter=",",skip_header = 2)
solI = interp1d(solarDat[:,0]*1E-9,solarDat[:,3])


# background refractive index
rrefvac = 1.0

# make a vector of wavelengths
lammin = 280.E-9
lammax = 1200.E-9
nlams = 1200
lams = np.linspace(lammin,lammax,nlams)
lams_nm = lams*1E9

# radius of the particle
rcore = 30E-9
rshell = 800.E-9

cabss = []
qabss = []
for lam in lams:
    xcore = 2.*np.pi*rcore*rrefvac/lam
    xshell= 2.*np.pi*rshell*rrefvac/lam
    rrefcore = nCeO2(lam) + kCeO2(lam)*1j
    rrefshell = nCeO2(lam) + kCeO2(lam)*1j
    [qext, qsca, qback, gsca] = bhcoat_pyed.bhcoat(xcore,xshell,rrefshell,rrefshell)
    cabss.append((qext-qsca)*np.pi*rshell**2.)
    qabss.append(qext-qsca)



plt.figure()
plt.plot(lams_nm,qabss,'-k', label='ceria abs efficiency factor')
plt.plot(lams_nm,solI(lams)/np.max(solI(lams)), label='solar spectrum')
plt.plot(lams_nm,planck(lams,temp)/np.max(planck(lams,temp)),'-m',label='blackbody emission')
plt.xlabel('wavelength [nm]')
plt.ylabel('Q_abs [1]')
plt.xlim(lammin*1E9,lammax*1E9)
plt.legend()


plt.figure()
emlams = np.linspace(300.E-9,4000.E-9,500)
plt.semilogx(emlams,planck(emlams,temp))

plt.show()

