# Description
This is a simple set of files to show how to use Git.

# How to run
To make the script run, the fortran code in the src folder needs to be compiled using f2py.  This is accomplished by invoking the pythonify.sh shell script.
